from . import db
from flask_login import UserMixin
import datetime
# from datetime import DateTime
from sqlalchemy.sql import func


class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_date = db.Column(db.Date)
    second_date = db.Column(db.Date)
    name_work = db.Column(db.String(150))
    count_days = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(150), unique=True)
    password = db.Column(db.String(150))
    first_name = db.Column(db.String(150))
    notes = db.relationship('Note')